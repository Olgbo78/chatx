import { ref } from "vue";
import { collection, onSnapshot } from "firebase/firestore";
import { useFirebaseCapabilities } from "./useFirebaseCapabilities";

const messages = ref([]);
const { db } = useFirebaseCapabilities();

export const useReadMessages = () => {
  const readChatMessages = (chatId) => {
    const chatRef = collection(db.value, "direct_chats", chatId, "messages");
    onSnapshot(chatRef, (snapshot) => {
      messages.value = [];
      snapshot.docs.forEach((doc) => {
        messages.value.push({
          ...doc.data(),
          text: doc.data().text,
          docId: doc.id,
          userId: doc.user_id,
        });
      });
      // calls the sort helper for messages
      messages.value.sort(compareFn);
    });
  };
  const readGroupChatMessages = (chatId) => {
    const groupRef = collection(db.value, "group_chats", chatId, "messages");
    onSnapshot(groupRef, (snapshot) => {
      messages.value = [];
      snapshot.docs.forEach((doc) => {
        messages.value.push({
          ...doc.data(),
          text: doc.data().text,
          docId: doc.id,
          userId: doc.user_id,
        });
      });
      // calls the sort helper for messages
      messages.value.sort(compareFn);
    });
  };
  return { readChatMessages, readGroupChatMessages, messages };
};

// sort helper
// TODO: make it work with the firestore sort
function compareFn(a, b) {
  if (a.timestamp < b.timestamp) {
    return -1;
  }
  if (a.timestamp > b.timestamp) {
    return 1;
  }
  // a must be equal to b
  return 0;
}
