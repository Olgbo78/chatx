import { ref } from "vue";
import { getFirestore } from "firebase/firestore";

const db = ref();
export const useFirebaseCapabilities = () => {
  const loadFirestore = () => {
    db.value = getFirestore();
  };
  return { loadFirestore, db };
};
