import { createRouter, createWebHistory } from "@ionic/vue-router";

import LoginView from "../pages/LoginView.vue";
import ChatView from "../pages/ChatView.vue";

const routes = [
  {
    path: "/",
    redirect: "/login",
  },
  {
    path: "/login",
    name: "login",
    component: LoginView
  },
  {
    path: "/chat",
    name: "chat",
    component: ChatView
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

// At each path it is checked if the route is 'login' AND if a token is available, if not the user is redirected to LoginView
router.beforeEach((to, from, next) => {
  const token = localStorage.getItem('token')
  if (to.name !== 'login' && !token) next({name: 'login'})
  else next();
});

export default router;
