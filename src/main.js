import { createApp } from 'vue'
import App from './App.vue'
import router from './router';

import { IonicVue } from '@ionic/vue';

/* Core CSS required for Ionic components to work properly */
import '@ionic/vue/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/vue/css/normalize.css';
import '@ionic/vue/css/structure.css';
import '@ionic/vue/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/vue/css/padding.css';
import '@ionic/vue/css/float-elements.css';
import '@ionic/vue/css/text-alignment.css';
import '@ionic/vue/css/text-transformation.css';
import '@ionic/vue/css/flex-utils.css';
import '@ionic/vue/css/display.css';

/* Theme variables */
import './theme/variables.css';
import './theme/core.css';

// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { useFirebaseCapabilities } from "./composables/useFirebaseCapabilities";

const { loadFirestore } = useFirebaseCapabilities();

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// apiKey muss sicherheitstechnisch ausgelagert werden
const firebaseConfig = {
  apiKey: "AIzaSyCxTLgAItvqKnTAt7AhVp4QYcGrENki3so",
  authDomain: "meetgram-af6fc.firebaseapp.com",
  projectId: "meetgram-af6fc",
  storageBucket: "meetgram-af6fc.appspot.com",
  messagingSenderId: "1015692548102",
  appId: "1:1015692548102:web:4ddf2ba5f16d18f58a8b87"
};

initializeApp(firebaseConfig);
loadFirestore();

const app = createApp(App)
  app.use(IonicVue)
  app.use(router);
  
router.isReady().then(() => {
  app.mount('#app');
});