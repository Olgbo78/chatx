# 1 Projektplanung 

## 1.1 Sollwerte für Qualität 

## 1.2 Ressourcen und Budget 
bla

# 2 Projektmanagement 

## 2.1 Gegenüberstellung Soll/Ist

# 3 Fachkonzept 

## 3.1 Fachlichen Grundlagen 

hybride Mobilapplikationen 
Native Apps 
PWA

### 3.1.1 Frameworks
Vue, Node, NPM, Ionic, Capacitor 

### 3.1.2 Backend / Datenbank 
Firebase, Firestore

### 3.1.3 IT-Security 

### 3.1.4 Testing 

## 3.2 Abläufe 

## 3.3 Use-Case Diagramme

## 3.4 Prototypings 

# 4 Technisches Konzept 

## 4.1 Gesamtarchitektur 

## 4.2 Struktur und Aufbau der Applikation 

## 4.3 etc. 

# 5 Teststrategie

## 5.1 Komponententest 

## 5.2 Integrationstest 

## 5.3 Performancetest

# 6 Fazit 
